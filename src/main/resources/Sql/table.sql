CREATE TABLE tb_category(
cate_id int primary KEY auto_increment,
cate_title VARCHAR(120)
);

CREATE TABLE tb_Article(
Article_ID int primary KEY auto_increment,
title   VARCHAR(120),
author  VARCHAR(120),
description   VARCHAR(100) NOT NULL,
thumbnail  VARCHAR(80) NOT NULL,
cate_id int REFERENCES  tb_category(cate_id)on DELETE CASCADE on UPDATE CASCADE);


Insert into  tb_category(cate_title) values('History');
Insert into  tb_category(cate_title) values('Biology');
Insert into  tb_category(cate_title) values('Math');
Insert into  tb_category(cate_title) values('WebDesign');
Insert into  tb_category(cate_title) values('WebBackend');

