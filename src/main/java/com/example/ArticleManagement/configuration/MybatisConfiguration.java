package com.example.ArticleManagement.configuration;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import javax.sql.DataSource;
@MapperScan("com.example.ArticleManagement.respository.ArticleRepo")
@Configuration
public class MybatisConfiguration {
    @Autowired
    DataSource dataSource;
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(){
        SqlSessionFactoryBean sql=new SqlSessionFactoryBean();
        sql.setDataSource(dataSource);
        return  sql;
    }
    @Bean
   public DataSourceTransactionManager dataSourceTransactionManager(){

        return new DataSourceTransactionManager(dataSource);
    }
}

