package com.example.ArticleManagement.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfiguration {
    @Bean
    @Profile("memory")
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder db = new EmbeddedDatabaseBuilder();
        db.setType(EmbeddedDatabaseType.H2);
        db.addScript("ClassPath:/Sql/table.sql");
        for(int i=0;i<=99;i++){
        db.addScript("ClassPath:/Sql/AddData.sql");}
        return db.build();}
    }
