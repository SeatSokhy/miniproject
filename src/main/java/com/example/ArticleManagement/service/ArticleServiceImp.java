package com.example.ArticleManagement.service;

import com.example.ArticleManagement.Pagination.Pagination;
import com.example.ArticleManagement.respository.ArticleRepo.ArticleRespository;
import com.example.ArticleManagement.respository.modal.ArticleFilte;
import com.example.ArticleManagement.respository.modal.ArticleModal;
import com.example.ArticleManagement.service.ServiceArticle.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    ArticleRespository articleRespository;

    @Override
    public List<ArticleModal> findall(ArticleFilte articleFilte, Pagination pagination) {
        pagination.setTotalCount(articleRespository.countAllArticles(articleFilte));
        return articleRespository.findall(articleFilte,pagination);
    }

    @Override
    public void insert(ArticleModal articleModal) {
        articleRespository.insert(articleModal);
    }
    @Override
    public void delete(int id) {
        articleRespository.delete(id);
    }
    @Override
    public void update(ArticleModal articleModal) {
        articleRespository.update(articleModal);
    }

    @Override
    public List<ArticleModal> ViewAll() {
        return articleRespository.ViewAll();
    }
    @Override
    public ArticleModal findByID(int id) {
        return articleRespository.findByID(id) ;
    }
}
