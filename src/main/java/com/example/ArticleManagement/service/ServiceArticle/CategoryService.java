package com.example.ArticleManagement.service.ServiceArticle;

import com.example.ArticleManagement.respository.modal.CategoryModal;

import java.util.List;

public interface CategoryService {
    void add(CategoryModal categoryModal);
    void update(CategoryModal categoryModal);
    void delete(int id);
    List<CategoryModal> showCate();
    public CategoryModal  findByID(int id);
}
