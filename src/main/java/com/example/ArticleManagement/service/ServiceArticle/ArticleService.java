package com.example.ArticleManagement.service.ServiceArticle;

import com.example.ArticleManagement.Pagination.Pagination;
import com.example.ArticleManagement.respository.modal.ArticleFilte;
import com.example.ArticleManagement.respository.modal.ArticleModal;

import java.util.List;

public interface ArticleService {
    public void insert(ArticleModal articleModal);

    public void delete(int id);

    public void update(ArticleModal articleModal);

    public List<ArticleModal> ViewAll();

    public ArticleModal findByID(int id);

    public List<ArticleModal> findall(ArticleFilte articleFilte, Pagination pagination);

}
