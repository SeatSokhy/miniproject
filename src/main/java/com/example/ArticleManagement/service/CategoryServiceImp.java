package com.example.ArticleManagement.service;

import com.example.ArticleManagement.respository.ArticleRepo.CategoryRespository;
import com.example.ArticleManagement.respository.modal.CategoryModal;
import com.example.ArticleManagement.service.ServiceArticle.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    CategoryRespository categoryRespository;
    @Autowired
    public CategoryServiceImp(CategoryRespository categoryRespository) {
        this.categoryRespository = categoryRespository;
    }

    @Override
    public void add(CategoryModal categoryModal) {
        categoryRespository.add(categoryModal);
    }

    @Override
    public void update(CategoryModal categoryModal) {
        categoryRespository.update(categoryModal);
    }

    @Override
    public void delete(int id) {
    categoryRespository.delete(id);
    }

    @Override
    public List<CategoryModal> showCate() {
        return categoryRespository.showCate();
    }

    @Override
    public CategoryModal findByID(int id) {
        return  categoryRespository.findByID(id)  ;
    }
}
