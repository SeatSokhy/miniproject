package com.example.ArticleManagement.controller;

import com.example.ArticleManagement.respository.modal.CategoryModal;
import com.example.ArticleManagement.service.ServiceArticle.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @GetMapping("/ViewCategory")
    public String cateView(ModelMap modelMap) {
        modelMap.addAttribute("view", categoryService.showCate());
        return "CategoryIndex";

    }

    @GetMapping("/deletecate/{id}")
    public String delete(@PathVariable int id) {
        System.out.println(id);
        categoryService.delete(id);
        return "redirect:/ViewCategory";
    }

    @PostMapping("/addcate")
    public String add( @ModelAttribute CategoryModal categoryModal) {
        categoryService.add(categoryModal);
        return "redirect:/ViewCategory";
    }

    @GetMapping("/updatecate/{id}")
    public String update(ModelMap modelMap, @PathVariable int id) {
        modelMap.addAttribute("viewobj", categoryService.findByID(id));
        return "CateUpdate";
    }

    @PostMapping("/cateupdate")
    public String Update(@Valid @ModelAttribute("viewobj") CategoryModal categoryModal,BindingResult bindingResult, ModelMap modelMap) {
        if (bindingResult.hasErrors()){
           modelMap.addAttribute("viewobj",categoryModal);
            return ("CateUpdate");
        }
        categoryService.update(categoryModal);
        return "redirect:/ViewCategory";
    }
}
