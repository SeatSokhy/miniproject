package com.example.ArticleManagement.controller;

import com.example.ArticleManagement.Pagination.Pagination;
import com.example.ArticleManagement.respository.modal.ArticleFilte;
import com.example.ArticleManagement.respository.modal.ArticleModal;
import com.example.ArticleManagement.service.ServiceArticle.ArticleService;
import com.example.ArticleManagement.service.ServiceArticle.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class ArticleController {
    @Value("${server.path}")
    String Serverpath;
    @Autowired
    ArticleService articleService;
    @Autowired
    CategoryService categoryService;

    @GetMapping(value = { "/index", "/article", "/all", "/" })
    public String Showall(ArticleFilte articleFilte, Pagination pagination, ModelMap modelMap) {
        modelMap.addAttribute("filte", articleFilte);
        modelMap.addAttribute("category", categoryService.showCate());
        modelMap.addAttribute("view", articleService.findall(articleFilte, pagination));
        return "index";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        modelMap.addAttribute("Article", new ArticleModal());
        modelMap.addAttribute("category", categoryService.showCate());
        return "AddDatafrm";
    }

    @PostMapping("/add")
    public String posartticle(@Valid @ModelAttribute("Article") ArticleModal modelAttribute, BindingResult result,
            @RequestParam MultipartFile files, ModelMap modelMap) {
        if (result.hasErrors()) {
            System.err.println("error");
            System.out.println(modelAttribute.getTitle());
            modelMap.addAttribute("Article", modelAttribute);
            modelMap.addAttribute("category", categoryService.showCate());
            System.out.println(result.getModel());
            return "AddDatafrm";
        }

        if (!files.isEmpty()) {
            String extention = files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf("."));
            String fileName = UUID.randomUUID().toString() + extention;
            try {
                Files.copy(files.getInputStream(), Paths.get(Serverpath, fileName));
            } catch (IOException e) {
                System.out.println(e);
            }
            modelAttribute.setThumbnail("/image/" + fileName);
        } else {
            modelAttribute.setThumbnail("/image/profile.png");
        }
        System.err.println(modelAttribute);
        articleService.insert(modelAttribute);
        return "redirect:/add";
    }

    @GetMapping("/update/{id}")
    public String Update(ModelMap modelMap, @PathVariable int id) {
        modelMap.addAttribute("article", articleService.findByID(id));

        modelMap.addAttribute("category", categoryService.showCate());
        return "Updateindex";
    }

    @PostMapping("/ActionUpdate")
    public String ActionUpdate(@Valid @ModelAttribute("article") ArticleModal articleModal, BindingResult result,
            ModelMap modelMap,  @RequestParam MultipartFile files) {
        if (result.hasErrors()) {
            modelMap.addAttribute("article", articleModal);
            modelMap.addAttribute("category", categoryService.showCate());
            return "Updateindex";
        }

        if (!files.isEmpty()) {
            String extention = files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf("."));
            String fileName = UUID.randomUUID().toString() + extention;
            try {
                Files.copy(files.getInputStream(), Paths.get(Serverpath, fileName));
            } catch (IOException e) {
                System.out.println(e);
            }
            articleModal.setThumbnail("/image/" + fileName);
        } else {
            articleModal.setThumbnail(articleService.findByID(articleModal.getId()).getThumbnail());
        }
        articleService.update(articleModal);
        return "redirect:/all";
    }

    @GetMapping("/remove")
    public String Delete(@RequestParam("id") int id) {
        System.out.println(id);
        articleService.delete(id);
        return "redirect:/all";
    }

    @GetMapping("/view/{id}")
    public String View(ModelMap map, @PathVariable int id) {
        map.addAttribute("article", articleService.findByID(id));

        return "ViewFrm";
    }
}
