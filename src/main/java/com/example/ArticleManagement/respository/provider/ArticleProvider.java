package com.example.ArticleManagement.respository.provider;

import com.example.ArticleManagement.Pagination.Pagination;
import com.example.ArticleManagement.respository.modal.ArticleFilte;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    public String findallData(@Param("filter")ArticleFilte articleFilte,@Param("pagin") Pagination pagination){
        return  new SQL(){{
            SELECT("*,tb_category.cate_title as titles");FROM("tb_article");
    
            INNER_JOIN("tb_category  ON tb_article.cate_id=tb_category.cate_id ");
            if (articleFilte.getTitle()!=null){
                WHERE("tb_article.title ILIKE'%'|| #{filter.title}||'%'");
            }
            if (articleFilte.getId()!=null){
                WHERE("tb_article.cate_id =#{filter.id}");
            }
            ORDER_BY("tb_article.article_id  LIMIT #{pagin.limit} OFFSET #{pagin.offset}");
        }}.toString();
    }
    public String countAllArticle(ArticleFilte articleFilte){

        return new SQL(){{
            SELECT ("COUNT(article_id) ");
            FROM("tb_article");
            if (articleFilte.getTitle()!=null){
                WHERE("title ILIKE'%'|| #{title}||'%'");
            }
            if (articleFilte.getId()!=null){
                WHERE("cate_id =#{id}");
            }
        }}.toString();
    }
}
