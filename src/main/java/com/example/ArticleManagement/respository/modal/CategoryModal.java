package com.example.ArticleManagement.respository.modal;

import javax.validation.constraints.NotBlank;

public class CategoryModal {
    Integer cate_id;
    @NotBlank
    String cate_title;

    public CategoryModal() {
    }

    public CategoryModal(Integer cate_id, String cate_title) {
        this.cate_id = cate_id;
        this.cate_title = cate_title;
    }
    public Integer getCate_id() {
        return cate_id;
    }

    public void setCate_id(Integer cate_id) {
        this.cate_id = cate_id;
    }

    public String getCate_title() {
        return cate_title;
    }

    public void setCate_title(String cate_title) {
        this.cate_title = cate_title;
    }

    @Override
    public String toString() {
        return "CategoryModal{" +
                "cate_id=" + cate_id +
                ", cate_title='" + cate_title + '\'' +
                '}';
    }
}
