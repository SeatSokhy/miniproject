package com.example.ArticleManagement.respository.modal;

public class ArticleFilte{
    Integer id;
    String title; 
    public ArticleFilte(){

    }
    public ArticleFilte( Integer id,String title){
        this.id=id;
        this.title=title;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}