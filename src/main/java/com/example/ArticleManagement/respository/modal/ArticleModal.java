package com.example.ArticleManagement.respository.modal;

import javax.validation.constraints.NotBlank;

public class ArticleModal {
    int id;
    @NotBlank
    String title;
    @NotBlank
    String author;
    @NotBlank
    String description;
    String thumbnail;
    CategoryModal categoryModal;

    public ArticleModal() {
    }

    public ArticleModal(int id, String title, String author, String description, String thumbnail,
            CategoryModal categoryModal) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.categoryModal = categoryModal;
    }

    public CategoryModal getCategoryModal() {
        return categoryModal;
    }

    public void setCategoryModal(CategoryModal categoryModal) {
        this.categoryModal = categoryModal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "ArticleModal{" + "id=" + id + ", title='" + title + '\'' + ", author='" + author + '\''
                + ", description='" + description + '\'' + ", thumbnail='" + thumbnail + '\'' + ", categoryModal="
                + categoryModal + '}';
    }
}
