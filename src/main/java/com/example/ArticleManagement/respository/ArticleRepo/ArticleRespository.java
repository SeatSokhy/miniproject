package com.example.ArticleManagement.respository.ArticleRepo;

import com.example.ArticleManagement.Pagination.Pagination;
import com.example.ArticleManagement.respository.modal.ArticleFilte;
import com.example.ArticleManagement.respository.modal.ArticleModal;
import com.example.ArticleManagement.respository.provider.ArticleProvider;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRespository {
        @Insert("INSERT INTO tb_article(title,author,description,thumbnail,cate_id) VALUES(#{title},#{author},#{description},#{thumbnail},#{categoryModal.cate_id})")
        public void insert(ArticleModal articleModal);

        @Delete("delete FROM tb_article WHERE article_id=#{id} ")
        public void delete(int id);

        @Update("update tb_article set title=#{title},author=#{author},description=#{description},cate_id=#{categoryModal.cate_id},thumbnail=#{thumbnail} where article_id=#{id}")
        public void update(ArticleModal articleModal);

        @Select("SELECT tb_article.*, tb_category.cate_title as titles FROM tb_article INNER JOIN tb_category ON tb_article.cate_id=tb_category.cate_id order by tb_article.cate_id ")
        @Results({ @Result(property = "id", column = "article_id"),
                        @Result(property = "categoryModal.cate_title", column = "titles") })
        public List<ArticleModal> ViewAll();

        @Select("SELECT tb_article.* ,tb_category.cate_title  as titles ,tb_category.cate_id as ids  from tb_article JOIN tb_category ON tb_article.cate_id=tb_category.cate_id where tb_article.article_id=#{id}")
        @Results({ @Result(property = "id", column = "article_id"),
                        @Result(property = "categoryModal.cate_title", column = "titles"),
                        @Result(property = "categoryModal.cate_id", column = "ids") })
        public ArticleModal findByID(int id);

        @SelectProvider(method = "findallData", type = ArticleProvider.class)
        @Results({ @Result(property = "id", column = "article_id"),
        @Result(property = "categoryModal.cate_title", column = "titles") })
        public List<ArticleModal> findall(@Param("filter") ArticleFilte articleFilte,@Param("pagin") Pagination pagination);
       @SelectProvider(method = "countAllArticle",type =ArticleProvider.class)
        public Integer countAllArticles(ArticleFilte articleFilte);

}
