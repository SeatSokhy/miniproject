package com.example.ArticleManagement.respository.ArticleRepo;

import com.example.ArticleManagement.respository.modal.CategoryModal;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRespository {
  @Insert("INSERT INTO tb_category (cate_title)VALUES(#{cate_title})")
  void add(CategoryModal categoryModal);
  @Update("update tb_category set cate_title= #{cate_title} where cate_id=#{cate_id}  ")
  void update(CategoryModal categoryModal);
  @Delete("DELETE FROM tb_category where cate_id=#{id}")
  void delete(int id);
  @Select("SELECT * FROM tb_category")
  List<CategoryModal> showCate();
  @Select("Select * from tb_category where cate_id=#{id} ")
   CategoryModal  findByID(int id);
}
